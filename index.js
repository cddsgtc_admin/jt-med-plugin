import jt from './src/utils/capture'
import canvas from './src/components/canvas.vue'

/**江泰插件 */
const jtPlugin = {
  install (vue, option) {
    // 设置截图
    // vue.prototype.$jt = {}
    vue.prototype.$jt = jt
    // 注册全局组件
    vue.component('jt-sign', canvas)
  }
}

// 如果存在则自动添加
if (window.Vue) {
  window.Vue.use(jtPlugin)
}

export default jtPlugin

export {capture, getImageFile} from './src/utils/capture'
export {
  canvas
}


