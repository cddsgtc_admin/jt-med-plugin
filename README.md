# jt-med-plugin

## 安装

```cmd
npm i jt-med-plugin

```

## 结构

## 示例

```js
// 引入
import jt from 'jt-med-plugin'

Vue.use(jt) //此时引入后会注册一个全局的`$jt`命名空间，可以使用`this.$jt.capture()`方法来截图

//在组件中可直接使用this.$jt.capture方法来使用
```

单独引入

```js
import { capture } from 'jt-med-plugin'
```

说明

该插件会在 vue 的原型定义一个`$jt`属性，携带一些常用方法。

方法

### 方法 capture

- 参数
  1. params: {target:HTMLELEMENT,isBlob?:boolean=false,type?:string='image/png',justPage:boolean=false}
  2. options: [查看](https://html2canvas.hertzen.com/configuration)
- 返回 Promise<dataURL> 如果没有报错返回的是 dataurl 字符

### 方法 getImageFile

截取目标 dom 并且转成文件对象，方便进行上传。

- 参数 {target:HTMLElement,name?:string,quality?:number,type?:string}
  参数是个对象

  - target 目标 dom
  - name 文件名称，默认日期数字，例如 20200814121259.png
  - quality 图片质量，0-1 之间，默认 0.8
  - type 文件类型，默认'image/png'
  - justPage:boolean=false 是否单纯截取全屏

- 返回 `Promise<File>` 内容是文件对象的 promise 对象。

示例:

```html
<template>
  <div class="test">
    <button @click="screenshot">截图</button>
  </div>
</template>

<script>

  methds:{
    async screenshot(){
     let imgUrl =  asait this.$jt.capture({target:document.body})
     let file = await this.$jt.getImageFile({target:document.body,name:'jt-med.png'})
    }
  }
</script>
```

## 组件`jt-sign`

该组件是一个在一个区域内实现触屏画画的功能，可用于签名等功能。

使用组件需要引入样式，如示：

```js
import 'jt-med-plugin/pro_dist/jt.css'
import jt from 'jt-med-plugin'

Vue.use(jt)
```

使用

```html
<div class='sign-container' style='height:500px;width:500px;>
  <jt-sign ref='sign'/>
</div>
```

插件会全局自动注册`jt-sign`组件，然后需要在页面该组件的外面添加一个容器，设置宽高。
也可以直接在该组件上设置样式宽高。

![示例](https://cdd-test.oss-cn-beijing.aliyuncs.com/for-test/jt_sign_case.gif)

组件 props

1. bg:string 背景颜色默认`rgba(255,255,250,1)`.
2. lineWidth:number 画笔的粗细程度，默认 1.5

组件属性：

1. background: 画布背景颜色可进行修改设定

组件方法：

1. getImage():Promise<DataURL>

- 返回 promise，内容是图片地址

```js
 async setImg(){
   this.imgurl = await this.$sign.getImage()
 }
```

2. getFile(name:string):Promise<File>

- 参数为文件名称
- 返回 Promise，内容是 file 对象，file 是图片，可用于上传

```js
async getFile(){
  return await this.$sign.getFile('med-sign.png')
}
```

3. clear():void 清除绘画的内容

示例

```js
const sign = this.$refs.sign
sign.clear()
```

4. back():void 清除最新的笔画

5. initBg():vlid 初始化画布背景
