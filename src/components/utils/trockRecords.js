var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var actionType;
(function (actionType) {
    actionType[actionType["start"] = 0] = "start";
    actionType[actionType["line"] = 1] = "line";
    actionType[actionType["finished"] = 2] = "finished";
})(actionType || (actionType = {}));
// 动作类
var Actions = /** @class */ (function () {
    function Actions(params) {
        this.records = [];
        this.background = 'rgba(255,255,250,1)';
        this.isEndLind = false;
        this.lineWidth = 1.5;
        this.canvas = params.canvas;
        this.painter = params.ctx || this.canvas.getContext('2d');
        params.background && (this.background = params.background);
        params.lineWidth && (this.lineWidth = params.lineWidth);
        this.initBg();
    }
    // 获取step
    Actions.prototype._steps = function () {
        var len = this.records.length;
        if (len) {
            var arr_1 = [];
            var o_1 = {};
            this.records.forEach(function (action, index) {
                if (action.type === actionType.start) {
                    o_1.start = index;
                }
                if (action.type === actionType.finished) {
                    o_1.end = index;
                    arr_1.push(Object.assign({}, o_1));
                    o_1 = {};
                }
            });
            return arr_1;
        }
        return [];
    };
    // 添加
    Actions.prototype.add = function (action) {
        this.records.splice(this.records.length, 0, action);
    };
    // 重新绘制，可用用back之中
    Actions.prototype.reDraw = function () {
        var _a, _b;
        this.painter.lineWidth = this.lineWidth;
        var steps = this._steps();
        if (steps.length > 0) {
            this.initBg();
            for (var _i = 0, steps_1 = steps; _i < steps_1.length; _i++) {
                var stpe = steps_1[_i];
                var actions = this.records.slice(stpe.start, stpe.end + 1);
                for (var _c = 0, actions_1 = actions; _c < actions_1.length; _c++) {
                    var _action = actions_1[_c];
                    switch (_action.type) {
                        case actionType.start:
                            this.painter.beginPath();
                            (_a = this.painter).moveTo.apply(_a, _action.point);
                            break;
                        case actionType.finished:
                            this.painter.stroke();
                            break;
                        default:
                            (_b = this.painter).lineTo.apply(_b, _action.point);
                    }
                }
            }
        }
    };
    // 回退一步
    Actions.prototype.back = function () {
        if (this.records.length > 0) {
            var steps = this._steps();
            var len = steps.length - 1;
            if (len === 0) {
                return this.clear();
            }
            var lastStep = steps[len - 1];
            this.records = this.records.slice(0, lastStep.end + 1);
            this.reDraw();
        }
    };
    Actions.prototype._wipeOver = function () {
        this.painter.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.initBg();
    };
    //绘制方法
    Actions.prototype.draw = function (action) {
        var none = this.records.length === 0;
        var hasOne = this.records.length === 1 && this.records[0].type === actionType.start;
        // 如果有一个则设置
        if (hasOne) {
            this.prePoint = this.records[0].point;
        }
        // 如果没有或者只有一个且是start则返回
        if (none || hasOne)
            return;
        var temp = Object.assign({}, this.prePoint);
        var point = action.point;
        this.prePoint = point;
        this.painter.lineWidth = this.lineWidth;
        switch (action.type) {
            case actionType.start:
                break;
            case actionType.finished:
                var steps = this._steps();
                this._drawFragment(steps[steps.length - 1]);
                break;
            default:
                this.painter.beginPath();
                this.painter.moveTo(temp.x, temp.y);
                this.painter.lineTo(point.x, point.y);
                this.painter.stroke();
        }
    };
    // 绘制片断
    Actions.prototype._drawFragment = function (fragment) {
        var _this = this;
        var points = this.records.slice(fragment.start, fragment.end + 1);
        this.painter.lineWidth = this.lineWidth;
        points.forEach(function (action) {
            var _a, _b, _c;
            var point = action.point;
            switch (action.type) {
                case actionType.start:
                    _this.painter.beginPath();
                    (_a = _this.painter).moveTo.apply(_a, point);
                    break;
                case actionType.finished:
                    (_b = _this.painter).lineTo.apply(_b, point);
                    _this.painter.stroke();
                    break;
                default:
                    (_c = _this.painter).lineTo.apply(_c, point);
            }
        });
    };
    // 初始化背景
    Actions.prototype.initBg = function () {
        var ctx = this.painter;
        ctx.fillStyle = this.background;
        ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    };
    // 清除画布
    Actions.prototype.clear = function (keep) {
        if (keep === void 0) { keep = false; }
        this.records = [];
        this._wipeOver();
    };
    return Actions;
}());
// 动作
var Action = /** @class */ (function () {
    function Action(type, point) {
        this.type = type;
        this.point = point;
    }
    return Action;
}());
// 点
var Point = /** @class */ (function (_super) {
    __extends(Point, _super);
    function Point(x, y) {
        var _this = _super.call(this, 2) || this;
        _this[0] = _this.x = x;
        _this[1] = _this.y = y;
        return _this;
    }
    return Point;
}(Array));
export { Action, Point, actionType };
export default Actions;
