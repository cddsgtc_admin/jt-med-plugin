export default function blobToFile (blob: Blob, fileName: string, type = "image/png") {
  return new File([ blob ], fileName, {
    type
  });
}
