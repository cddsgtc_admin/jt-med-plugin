var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import blobToFile from "./blobToFile";
import toCanvas from "html2canvas";
import { Time } from "cdd-lib";
var ToImage = /** @class */ (function () {
    function ToImage() {
    }
    /**
     * 获取图片或者上传blob
     * @param {object} params 参数
     * @param {HTMLElement} params.target 需要生成图片的dom
     * @param {boolean} params.isBlob 是否返回blob
     * @param {string} params.type 图片类型
     * @return Promise<string|Blob> 返回promise
     */
    ToImage.generate = function (params, options) {
        // 初始数据
        params = Object.assign({}, { isBlob: false, type: 'image/png', justPage: false }, params);
        options = options || {};
        if (params.justPage) {
            options.height = screen.height;
        }
        return new Promise(function (reso, rej) {
            toCanvas(params.target, options).then(function (canvas) {
                if (params.isBlob) {
                    return canvas.toBlob(function (blob) {
                        if (blob) {
                            reso(blob);
                        }
                        else {
                            rej(blob);
                        }
                    }, params.type);
                }
                else {
                    reso(canvas.toDataURL());
                }
            });
        });
    };
    // 获取文件
    ToImage.getImageFile = function (params, options) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var name, time, blob;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        params = Object.assign({}, { isBlob: true, quality: .8, type: 'image/png' }, params);
                        name = params.name;
                        // 如果没有名字则进行自定义名字
                        if (!params.name) {
                            time = new Time({ dateSeparator: '', timeSeparator: '' });
                            name = time.dateStr + time.timeStr + '.' + ((_a = params.type) === null || _a === void 0 ? void 0 : _a.split('/')[1]);
                        }
                        return [4 /*yield*/, ToImage.generate(params, options)];
                    case 1:
                        blob = _b.sent();
                        return [4 /*yield*/, blobToFile(blob, name)];
                    case 2: return [2 /*return*/, _b.sent()];
                }
            });
        });
    };
    return ToImage;
}());
var capture = ToImage.generate;
var getImageFile = ToImage.getImageFile;
export { capture, getImageFile };
export default {
    capture: capture,
    getImageFile: getImageFile
};
