export default function blobToFile(blob, fileName, type) {
    if (type === void 0) { type = "image/png"; }
    return new File([blob], fileName, {
        type: type
    });
}
