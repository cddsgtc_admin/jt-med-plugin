import Vue from 'vue'
import App from './App.vue'
import jt from '../pro_dist/jt.umd.min'

Vue.use(jt)
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
